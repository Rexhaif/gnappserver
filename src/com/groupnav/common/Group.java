package com.groupnav.common;

import java.io.IOException;
import java.util.List;


public class Group {
    public String getName() {
        return name;
    }

    private User master;

    private String name;

    public void setSlaves(List<User> slaves) {
        this.slaves = slaves;
    }

    private List<User> slaves;

    private String targetPosition;

    public Group(User master, List<User> slaves, String target, String name) {
        this.name = name;
        this.master = master;
        this.slaves = slaves;
        this.targetPosition = target;
    }

    public List<User> getSlaves() {
        return slaves;
    }

    public User getMaster() {

        return master;
    }

    public String getTargetPosition() {
        return targetPosition;
    }


    public void sendMessageForEach(String msg){
        List<User> tList = slaves;
        tList.add(master);
        tList.forEach((v) -> {
            try {
                v.getConnection().sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


}
