package com.groupnav.common;

import org.eclipse.jetty.websocket.WebSocket;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Даниил on 24.03.2015.
 */
public class User implements Serializable {

    private String mEmail;

    private String mPassword;

    private transient boolean isOnline = false;

    private transient WebSocket.Connection mConnection = null;

    private transient float[] currPosition;

    public User(String email, String password, boolean online) {
        mEmail = email;
        mPassword = password;
        isOnline = online;
        mConnection = null;
        currPosition = new float[2];
    }
    public User(String email, String password, boolean online, WebSocket.Connection connection) {
        mEmail = email;
        mPassword = password;
        isOnline = online;
        mConnection = connection;
        currPosition = new float[2];
    }


    public void setConnection(WebSocket.Connection connection) {
        mConnection = connection;
    }

    public WebSocket.Connection getConnection() {
        return mConnection;
    }

    public void setCurrPosition(float[] position) {
        currPosition = position;
    }

    public float[] getCurrPosition(){
        return currPosition;
    }

    public boolean validatePass(String password){
        return password.equals(mPassword);
    }

    public String getEmail() {
        return mEmail;
    }

    public boolean checkOnline() {
        return isOnline;
    }

    public void setOnline(){
        isOnline = true;
    }

    public void setOffline(){
        isOnline = false;
    }

}
