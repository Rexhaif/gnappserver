package com.groupnav.websocketserver;

import com.groupnav.common.Group;
import com.groupnav.common.User;

import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;


public class GroupHandler extends Thread {

    private Group mGroup;

    private Queue<Map.Entry<User, JSONObject>> messageQueue;

    private List<User> acceptedUsers;

    private boolean runFlag = true;

    public GroupHandler(Group group) {
        this.mGroup = group;
        messageQueue = new LinkedBlockingQueue<>();
        acceptedUsers = new ArrayList<>(15);
        group.getSlaves().forEach((v) -> {
            try {
                v.getConnection().sendMessage(
                        new JSONObject()
                                .put("from", mGroup.getMaster().getEmail())
                                .put("operation", "groupInvite")
                                .put("name", mGroup.getName())
                                .put("target", mGroup.getTargetPosition())
                                .toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }



    @Override
    public void run() {


        while (runFlag) {
            Map.Entry<User, JSONObject> tMsg = messageQueue.poll();
            if (tMsg == null) {
                continue;
            }
            String groupOperation = tMsg.getValue().getString("groupOperation");
            if (groupOperation.equals("updateCoords")) {
                ArrayList<Double> coords = new ArrayList<>();
                coords.add(tMsg.getValue().getJSONArray("parameter")
                        .getDouble(0));
                coords.add(tMsg.getValue().getJSONArray("parameter")
                        .getDouble(1));

                mGroup.sendMessageForEach(
                        new JSONObject()
                        .put("from", tMsg.getKey().getEmail())
                        .put("operation", "updateCoords")
                        .put("coords", coords)
                        .toString()
                        );
            } else if (groupOperation.equals("message")) {
                mGroup.sendMessageForEach(
                        new JSONObject()
                            .put("from", tMsg.getKey().getEmail())
                            .put("operation", "message")
                            .put("text", tMsg.getValue().getString("parameter"))
                            .toString()
                );

            } else if (groupOperation.equals("leave")) {
                mGroup.sendMessageForEach(
                        new JSONObject()
                            .put("from", tMsg.getKey().getEmail())
                            .put("operation", "leave")
                            .put("reason", tMsg.getValue().getString("parameter"))
                            .toString()
                );
            } else if (groupOperation.equals("inviteResponse")) {
                if (tMsg.getValue().getString("parameter").equals("accept")) {
                    acceptedUsers.add(tMsg.getKey());
                    mGroup.setSlaves(acceptedUsers);
                }
            }
        }

        super.run();
    }

    @Override
    public void interrupt() {
        mGroup.sendMessageForEach(new JSONObject()
                .put("from", mGroup.getMaster().getEmail())
                .put("operation", "groupDestroy")
                .toString()
        );
        runFlag = false;
        super.interrupt();
    }

    public void onRecieve(User from, JSONObject message) {
        messageQueue.add(new MessageEntry(from, message));
    }



    private class MessageEntry implements Map.Entry<User, JSONObject> {

        private User key;

        private JSONObject value;

        public MessageEntry(User user, JSONObject message) {
            this.key = user;
            this.value = message;
        }

        @Override
        public User getKey() {
            return key;
        }

        @Override
        public JSONObject getValue() {
            return value;
        }

        @Override
        public JSONObject setValue(JSONObject value) {
            return null;
        }


    }
}
