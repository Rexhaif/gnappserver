package com.groupnav.websocketserver;

import com.groupnav.common.Group;
import com.groupnav.common.User;
import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketHandler;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;


/**
 * Created by Даниил on 01.03.2015.
 */
public class GroupNavWSHandler extends WebSocketHandler {

    private Map<String, User> usersDB = new HashMap<String, User>(15);

    private Set<GroupNavWSConnection> webSockets = new HashSet<GroupNavWSConnection>(15);
    private Map<String, GroupHandler> groups = new HashMap<>(15);

    @Override
    public WebSocket doWebSocketConnect(HttpServletRequest httpServletRequest, String s) {

        return new GroupNavWSConnection();
    }

    public Map<String, User> getUsersDB() {
        return usersDB;
    }

    public class GroupNavWSConnection implements WebSocket.OnTextMessage {

        private Connection connection;

        private User user;

        @Override
        public void onMessage(String s) {
            connection.setMaxIdleTime(600000);
            JSONObject message = new JSONObject(s);
            if (!checkKeys(message, "operation")) {
                try {
                    connection.sendMessage("bad request");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                connection.close();
            } else {
                String operation = message.getString("operation");
                if (operation.equals("signUp")) {
                    try {
                        connection.sendMessage(processSignUp(message));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (operation.equals("logIn")) {
                    try {
                        connection.sendMessage(processLogIn(message));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (operation.equals("logOut")) {
                    try {
                        connection.sendMessage(processLogOut(message));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    connection.close();
                } else if (operation.equals("newGroup")){
                    try {
                        connection.sendMessage(processNewGroup(message));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (operation.equals("ping")) {
                    try {
                        connection.sendMessage(new JSONObject()
                                                        .put("result", "pong")
                                                        .toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (operation.equals("groupMsg")) {
                    try {
                        connection.sendMessage(processGroupMsg(message));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (operation.equals("removeGroup")) {
                    try {
                        connection.sendMessage(processRemoveGroup(message));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        public void onOpen(Connection connection) {
            this.connection = connection;
            webSockets.add(this);
        }

        @Override
        public void onClose(int i, String s) {
            webSockets.remove(this);
        }



        private String processSignUp(JSONObject message) {

            if (!checkKeys(message, "email", "password")) {
                return new JSONObject()
                        .put("result", "bad request")
                        .toString();
            } if (usersDB.containsKey(message.getString("email"))){
                return new JSONObject()
                        .put("result", "user already exist")
                        .toString();
            } else {
                user = new User(message.getString("email"),
                        message.getString("password"),
                        true,
                        connection);
                usersDB.put(message.getString("email"),
                        user);
                return new JSONObject()
                            .put("result", "success")
                            .toString();
            }

        }

        private String processLogIn(JSONObject message) {
            if (!checkKeys(message, "email", "password")) {
                return new JSONObject()
                        .put("result", "bad request")
                        .toString();
            } else {
                if (!usersDB.containsKey(message.getString("email"))) {
                    return new JSONObject()
                            .put("result", "user not exist")
                            .toString();
                }
                if (!usersDB
                        .get(message
                                .getString("email"))
                        .validatePass(message
                                .getString("password"))) {
                    return new JSONObject()
                            .put("result", "incorrect password")
                            .toString();
                }
                user = usersDB.get(message.getString("email"));
                user.setConnection(connection);
                user.setOnline();
                commit();
                return new JSONObject()
                        .put("result", "success")
                        .toString();
            }
        }

        private String processNewGroup(JSONObject message) {
            if (user == null) {
                return new JSONObject()
                        .put("result", "not authorized")
                        .toString();
            }
            if (!checkKeys(message, "target", "members", "name")) {
                return new JSONObject()
                        .put("result", "bad request")
                        .toString();
            }
            if (groups.containsKey(message.getString("name"))) {
                return new JSONObject()
                        .put("result", "name already exist")
                        .toString();
            }
            JSONArray members = message.getJSONArray("members");
            List<User> availableUsers = new ArrayList<User>(15);
            List<String> unavailableUsers = new ArrayList<String>(15);
            for (int i = 0; i < members.length(); ++i) {
                String member = members.getString(i);
                if (!usersDB.containsKey(member)) {
                    unavailableUsers.add(member);
                } else if (!usersDB.get(member).checkOnline()) {
                    unavailableUsers.add(member);
                } else {
                    availableUsers.add(usersDB.get(member));
                }
            }
            Group group = new Group(user,availableUsers, message.getString("target"), message.getString("name"));
            GroupHandler handler = new GroupHandler(group);
            handler.start();
            groups.put(message.getString("name"), handler);
            return new JSONObject()
                    .put("result", "success")
                    .put("offline", unavailableUsers)
                    .toString();

        }

        private String processGroupMsg(JSONObject message) {
            if (user == null) {
                return new JSONObject()
                        .put("result", "not authorized")
                        .toString();
            }
            if (!checkKeys(message, "name", "groupOperation", "parameter")) {
                return new JSONObject()
                        .put("result", "bad request")
                        .toString();
            } else {
                groups
                        .get(message.getString("name"))
                        .onRecieve(user, message);

                return new JSONObject()
                        .put("result", "process...")
                        .toString();
            }
        }



        private String processLogOut(JSONObject message) {
            if (user == null) {
                return new JSONObject()
                        .put("result", "user not logged in")
                        .toString();
            } else {
                user.setConnection(null);
                user.setOffline();
                commit();
                user = null;
                return new JSONObject()
                        .put("result", "success")
                        .toString();
            }
        }

        private String processRemoveGroup(JSONObject message) {
            if (user == null) {
                return new JSONObject()
                        .put("result", "not authorized")
                        .toString();
            } else  if (!checkKeys(message, "name")){
                return new JSONObject()
                        .put("result", "bad request")
                        .toString();
            } else {
                groups.get(message.getString("name")).interrupt();
                groups.remove(message.getString("name"));
                return new JSONObject()
                        .put("result", "remove in progress")
                        .toString();
            }
        }

        private void commit() {
            usersDB.replace(
                    user.getEmail(),
                    usersDB.get(user.getEmail()),
                    user
            );
        }

        private boolean checkKeys(JSONObject msg, String... keys) {
            for (String key : keys) {
                if ((!msg.keySet().contains(key))) return false;
                else if (msg.isNull(key)) return false;
            }
            return true;
        }


    }
}
