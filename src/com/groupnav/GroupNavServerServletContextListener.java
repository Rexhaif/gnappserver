package com.groupnav;

import com.groupnav.common.User;
import com.groupnav.websocketserver.GroupNavWSHandler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Даниил on 01.03.2015.
 */
public class GroupNavServerServletContextListener implements ServletContextListener {

    Server server;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        server = new Server(8081);
        GroupNavWSHandler handler = new GroupNavWSHandler();

        handler.setHandler(new DefaultHandler());
        server.setHandler(handler);
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

        if (server != null) {
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
